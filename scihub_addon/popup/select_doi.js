const btn = document.querySelector("#btn-search");
const sch = document.querySelector("input.search-term");
const err = document.querySelector("#p-error");
let selectedDOI = "";

btn.addEventListener("click", () => {
    var newTab = window.open('', '_blank');
    const resp = browser.storage.sync.get("sciurl");
    resp.then(
        (r) => {
            const url = `${r.sciurl || "https://sci-hub.st/"}${sch.value}`;
            newTab.location.href = url;
        }
    );
});

function processDOI() {
    const res = selectedDOI.match(/10\.\d{4,9}\/[\.a-zA-Z0-9:;\(\)-\/]+/);
    if (res) {
        sch.value = res[0];
    }
}

function onError(error) {
    err.textContent = error;
}

browser.runtime.onMessage.addListener(
    (data, sender) => {
        selectedDOI = data.selection;
    }
);

browser.tabs.executeScript({ file: "/content_scripts/scihubdoi.js" })
    .then(() => processDOI())
    .catch(onError);