(function() {
    const url = window.location.href;
    let text = "";
    if (/.*sciencedirect\.com.*/.test(url)) {
        text = document.querySelector("a.doi").getAttribute("href");
    } else if (/.*acs\.org.*/.test(url)) {
        text = document.querySelector('a[title="DOI URL"]').getAttribute("href");
    } else if (/.*springer\.com.*/.test(url)) {
        text = document.querySelector("li.c-bibliographic-information__list-item--doi a").getAttribute("href");
    } else if (/.*elibrary\.ru.*/.test(url)) {
        text = document.querySelector("body").innerHTML;
    } else {
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
    }
    browser.runtime.sendMessage({ selection: text });
})();