(() => {
    let getting = browser.storage.sync.get("remsb");
    getting.then(removeSidebar, onError);
})();

function removeSidebar(res) {
    const sidebar = document.querySelector("#menu");
    const article = document.querySelector("#article");

    if (res.remsb) {
        if (sidebar) {
            sidebar.style.display = "none";
        }

        if (article) {
            article.style.marginLeft = "0";
        }
    } else {
        if (sidebar) {
            sidebar.style.display = "inline";
        }

        if (article) {
            article.style.marginLeft = sidebar.style.width;
        }
    }
}

function onError(res) {
    console.log(res);
}