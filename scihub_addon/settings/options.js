const urlReg = /(https?:\/\/)?(www\.)?([a-zA-Z0-9@\-:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6})\b[-a-zA-Z0-9()@:%_\+.~#?&//=]*/;

function saveOptions(e) {
    e.preventDefault();
    const sciurlRow = document.querySelector("#sci-url").value;
    let res = sciurlRow.match(urlReg);
    if (res) {
        browser.storage.sync.set({
            sciurl: `https://${res[3]}/`
        });
    } else {
        document.querySelector("#error-log").value = "Can't match given URL!";
    }
}

function restoreOptions() {

    function setCurrentMirror(result) {
        if (result.sciurl) {
            document.querySelector("#sci-url").value = result.sciurl;
        } else {
            browser.storage.sync.set({ sciurl: "https://sci-hub.st/" });
            document.querySelector("#sci-url").value = "https://sci-hub.st/";
        }
    }

    function setSidebarStatus(result) {
        const cb = document.querySelector("#cbxHS");
        if (result.remsb) {
            cb.setAttribute("checked", "checked");
        } else {
            cb.removeAttribute("checked");
        }
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    browser.storage.sync.get("sciurl")
        .then(setCurrentMirror, onError);
    browser.storage.sync.get("remsb")
        .then(setSidebarStatus, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
document.querySelector("#sHS").addEventListener("click", () => {
    const cb = document.querySelector("#cbxHS");
    if (cb.getAttribute("checked")) {
        cb.removeAttribute("checked");
        browser.storage.sync.set({
            remsb: false
        });
    } else {
        cb.setAttribute("checked", "checked");
        browser.storage.sync.set({
            remsb: true
        });
    }
})